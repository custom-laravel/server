#!/bin/sh
# --------------------------------------------------------------------------------
# migrateバッチ
# --------------------------------------------------------------------------------

# 基本変数定義
HOME=`pwd`
DATE=`TZ=Asia/Tokyo date +%Y%m%d_%H%M%S`
LOG_FILE="/server/migrate/log/${DATE}.log"
APPLIED_FILE="/server/migrate/shell_applied/${DATE}.txt"

# Gitリポジトリ更新
echo '----- Git Update Start -----'
/usr/local/bin/git -C /server reset --hard HEAD
/usr/local/bin/git -C /server clean -fd
/usr/local/bin/git -C /server pull origin master
echo '----- Git Update End -----'

echo '----- Release Start -----'
# /migrate/shell_appliedのファイルの中身を行単位の配列(applied)で保持する
applied=(`find /server/migrate/shell_applied/ -type f -name "*.txt" | xargs cat`)
pending=()

# /migrate/shell_listのファイル一覧より、appliedにないものを配列(pending)で保持する
for file in `find /server/migrate/shell_list -type f -name "*.sh"`; do
    if ! `echo ${applied[@]} | grep -q "${file}"`; then
        pending+=("${file}")
    fi
done

# pendingのshellを実行していく (/migrate/log/YYYYMMdd_hhmmss.logにログを追記する)
# shell実行後、/migrate/shell_applied/YYYYMMdd_hhmmss.txtに実行したパスを追記していく
for file in ${pending[@]}; do
    echo ${file} running...
    echo "----- ${file} RUNNING -----" | tee -a ${LOG_FILE}
    eval ${file} >> ${LOG_FILE}
    if [ $? -ne 0 ] ; then
        echo "----- ${file} ERROR -----" | tee -a ${LOG_FILE}
        echo "----- migrate STOP -----" | tee -a ${LOG_FILE}
        exit 1
    fi
    echo "----- ${file} END -----" | tee -a ${LOG_FILE}
    echo ${file} >> ${APPLIED_FILE}
done
echo '----- Release End -----'

exit 0
