#!/bin/sh
# --------------------------------------------------------------------------------
# database batch
# --------------------------------------------------------------------------------

# 環境変数を変数として読み込み
source /server/application/.env

# DB実行
docker exec -i database mysql -u ${DB_USERNAME} -p${DB_PASSWORD} --connect-expired-password work < /server/migrate/shell_list/20181001/laravel.sql

exit 0;
